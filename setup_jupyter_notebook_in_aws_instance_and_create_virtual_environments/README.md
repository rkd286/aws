# AWS - Setup Jupyter Notebook in an AWS EC2 instance and Create Virtual Environments from Jupyter Terminal
We will see how to Setup Jupyter Notebook in an AWS instance (Ubuntu) and create Virtual Environments in the Jupyter Server.

## 1. Create EC2 Instance - Ubuntu
1. Under EC2 Service, go to Instances
2. Click on Launch Instance
3. Select your AMI. I chose the Ubuntu 18.04 one
![](img/ubuntu-18.04.PNG)<br /><br />
4. Select the free tier one
5. Click Next
6. Choose the VPC, the **Public Subnet** and auto-assign IP **Enabled**
7. Click Next, skip the storage and add Tags if you wish
8. Click Next and create a new Security Group, give it a name
9. Create a new rule - Type - TCP, **Port - 8888**(Since Jupyter server runs on port 8888), Source - 0.0.0.0/0
10. Select **Review and Launch**

## 2. Login to the Ubuntu Instance and Update
> *sudo apt update*

## 3. Install pip
> *sudo apt install python3-pip*

## 4. Now, install the Virtual Environment package
> *pip3 install virtualenv*

## 5, Create a Virtual Environment and Activate it
>*mkdir jupyter<br />
cd jupyter<br />
virtualenv -p python3 my_env<br />
source my_env/bin/activate*<br />

## 6. Install Jupyter
> *pip install jupyter*

## 7. Configure Jupyter Notebook
> *jupyter notebook --generate-config*

This creates a Configuration file in
> */home/username/.jupyter/jupyter_notebook_config.py*

## 8. Edit the Configuration File
> *sudo nano /home/username/.jupyter/jupyter_notebook_config.py*

Find **c.Notebook.ip** and remove the **#** and replace  **localhost** with **0.0.0.0**

Save and exit.

## 9. Generate a Password to avoid entering tokens everytime
> *jupyter notebook password*

 Enter the password.

## 10. Test the Setup
> *nohup jupyter notebook &*

**nohup** command is to ensure the server keeps running inspite of terminating the SSH session.

**&** is used to make the server run in the backgorund

Now, Login to your browser and you should see the Password Prompt.<br /><br />
![](img/login.PNG)<br />

## 11. Create Virtual Environments from your Jupyter Notebook
1. After logging in, on the top right, click on New > Terminal

2. Navigate to the directory where you created the previous environment
3. Create a new Virtual Environment
> *virtualenv -p python3 env1*

![](img/terminal.PNG)

4. activate the new environment and install ipykernel
> *pip install ipykernel<br />
python -m ipykernel install --user --name=env1*

5. The Environment is now setup. Install all the required libraries in this new environment.

6. You can now see the new environment appear under the **New** Tab.
![](img/env.PNG)<br />

**Hope you found this useful. Thank You!**
