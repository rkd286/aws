# AWS - Configure Jump Box and NAT Instance to connect to a Private Subnet
We will see how to connect to an instance in the Private Subnet of a VPC using a Jump Box/Server. A NAT instance is used for the private instance to connect to the internet. However, there will be no incoming traffic to this instance.

## Architecture
![](img/NAT-Architecture.png)<br /><br />


## 1. Create VPC
1. Go to Services/VPC
2. Click on Create VPC
3. Give it a name
4. Enter the IPv4 address range. In the CIDR block, enter 10.0.0.0/16 (/16 Preferred)
5. Click on Create<br /><br />

## 2. Create Private and Public Subnets
  * **Public Subnet**
    1. Under VPC, go to Subnets
    2. Click on create Subnet
    3. Give it a name
    4. Choose the VPC you created earlier
    5. Give it an IP address range of your choice. In the CIDR block, enter 10.0.1.0/24 (/24 Preferred)
    6. Click on Create<br /><br />

  * **Private Subnet**
    1. Under VPC, go to Subnets
    2. Click on create Subnet
    3. Give it a name
    4. Choose the VPC you created earlier
    5. Give it an IP address range of your choice. In the CIDR block, enter 10.0.3.0/24 (/24 Preferred)
    6. Click on Create<br /><br />

## 3. Create Internet Gateway
1. Under VPC, go to Internet Gateway
2. Give it a name
3. Click on Yes, Create
4. Now. select the Internet Gateway you just created from the list
5. On the top, click on Attach to VPC
6. Select the VPC you created
7. Ensure to copy and save the Internet Gateway ID in your notepad which will be under the Description tab at the bottom. The id will have a prefix 'igw'<br /><br />

## 4. Create EC2 Instances
  * **Linux Box**
    1. Under EC2, go to Instances
    2. Click on Launch Instance
    3. Select your AMI. I chose the Amazon Linux 2 AMI (First one)
    4. Select the free tier one
    5. Click Next
    6. Choose the VPC, the **Private Subnet** and auto-assign IP **Disabled**
    7. Click Next, skip the storage and add Tags if you wish
    8. Click Next and create a new Security Group, give it a name
    9. Keep the default settings and select **Review and Launch**<br /><br />

  * **NAT Instance**
    1. Under EC2, go to Instances
    2. Click on Launch Instance
    3. Select your AMI. I chose the Amazon Linux 2 AMI (First one)
    4. Select the free tier one
    5. Click Next
    6. Choose the VPC, the **Public Subnet** and auto-assign IP **Enabled**
    7. Click Next, skip the storage and add Tags if you wish
    8. Click Next and create a new Security Group, give it a name
    9. Allow all traffic, with Source - 0.0.0.0/0
    10. Select **Review and Launch**<br /><br />

  * **Jump Box**
    1. Under EC2, go to Instances
    2. Click on Launch Instance
    3. Select your AMI. I chose the Amazon Linux 2 AMI (First one)
    4. Select the free tier one
    5. Click Next
    6. Choose the VPC, the **Public Subnet** and auto-assign IP **Enabled**
    7. Click Next, skip the storage and add Tags if you wish
    8. Click Next and create a new Security Group, give it a name
    9. Keep the default settings and select **Review and Launch**<br /><br />

## 5. Create Route Table
  **We need to create two Route Tables**
  1. Under VPC, go to Route Tables
  2. Click on Create Route Table

  * **Public Route Table**
    1. Give it a name
    2. Selected the VPC you created
    3. Click on Yes, Create
    4. Now, select the Route Table you just created from the list
    5. In the bottom, Under the 'Routes' Tab, click Edit
    6. Enter the Destination as 0.0.0.0/0 and Paste the IG ID you noted earlier in your Notepad as the Target.
      ![](img/route-table-public.PNG)<br /><br />
    7. Click Save
    8. In the Subnet Associations Tab, click on Edit
    9. Choose the **Public Subnet** you created earlier<br /><br />

  * **Private Route Table**
    1. Give it a name
    2. Selected the VPC you created
    3. Click on Yes, Create
    4. Now, select the Route Table you just created from the list
    5. In the bottom, Under the 'Routes' Tab, click Edit
    6. Enter the Destination as 0.0.0.0/0 and Paste the ID of the NAT instance.
    ![](img/route-table-private.PNG)<br /><br />
    7. Click Save
    8. In the Subnet Associations Tab, click on Edit
    9. Choose the **Private Subnet** you created earlier<br /><br />

## 6. Test the Setup
* **Jump Box**
  1. Login to the Jump Box using Putty(Windows) or any SSH client
  2. We now need to transfer the .pem file to the Jump Server in order the SSH to the private NAT instance. Use this command:
  > scp -i "your_pem.pem" pem_to_transfer.pem ec2-user@public_ip:/tmp/pem_to_transfer.pem
  >

  3. Now SSH into the NAT instance and ping google.com. You should see the following response:
  ![](img/InkedCapture-ping_LI.jpg)<br /><br />

**Hope you found this useful. Thank You!**
